<?php

$app->get('/', function () use ($app) {
    $app->render('home.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Home']));
});

$app->get('/styleguide', function () use ($app) {
    $app->render('styleguide.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Home']));
});
$app->get('/login', function () use ($app) {
    $app->render('login.twig', \Straightline\MainController::login());
});

$app->group('/campaign', function () use ($app) {
    $app->get('/', function () use ($app) {
        $app->render('campaign.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Campaign', 'create' => 1]));
    });
    $app->get('/create', function () use ($app) {
        $app->render('campaignCreate.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Campaign', 'create' => 1]));
    });
    $app->get('/creatives/create', function () use ($app) {
        $app->render('creativesCreate.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Campaign', 'create' => 1]));
    });
    $app->get('/search', function () use ($app) {
        $app->render('campaignSearch.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Campaign Search']));
    });
});
$app->get('/favourites', function () use ($app) {
    $app->render('favourites.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Favourites']));
});

$app->get('/messages', function () use ($app) {
    $app->render('messages.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Messages']));
});
$app->group('/foreign', function () use ($app) {
    $app->get('/', function () use ($app) {
        $app->render('foreignExchange.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Foreign Exchange']));
    });
    $app->get('/add', function () use ($app) {
        $app->render('addForeignExchange.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Foreign Exchange Add']));
    });
});

$app->get('/insertionOrders', function () use ($app) {
    $app->render('insertionOrders.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Insertion Orders']));
});
$app->get('/insertionOrdersReserve', function () use ($app) {
    $app->render('insertionOrdersReserve.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Insertion Orders Reserve']));
});
$app->get('/insertionOrdersSearch', function () use ($app) {
    $app->render('insertionOrdersSearch.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Insertion Orders Search']));
});
$app->get('/userProfile', function () use ($app) {
    $app->render('userProfile.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'User Profile']));
});
$app->get('/campaignDropDown', function () use ($app) {
    $app->render('userProfile.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Campaigns Drop Down']));
});
$app->get('/messagesDropDown', function () use ($app) {
    $app->render('messagesDropDown.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Messages Drop Down']));
});
$app->get('/favouritesDropDown', function () use ($app) {
    $app->render('favouritesDropDown.twig', \Straightline\MainController::basicSettings(['pageTitle' => 'Favourites Drop Down']));
});
