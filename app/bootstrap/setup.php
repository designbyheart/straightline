<?php

defined("ROOT_DIR") ? null : define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"] . '/current/');
defined("SITE_ROOT") ? null : define("SITE_ROOT", 'http://' . $_SERVER["SERVER_NAME"] . '/');
defined("galleryURL") ? null : define("galleryURL", 'http://' . $_SERVER["SERVER_NAME"] . '/gallery/');
defined("galleryROOT") ? null : define("galleryROOT", str_replace('/current/', '', ROOT_DIR) . '/gallery/');

require_once(ROOT_DIR . 'vendor/autoload.php');

//if ($_SERVER['SERVER_NAME'] == 'straightline.io') {
//ORM::configure('mysql:host=127.0.0.1;dbname=rentacar');
//ORM::configure('username', 'root');
//ORM::configure('password', 'root');
//} else {
//ORM::configure('mysql:host=localhost;dbname=rentacar');
//ORM::configure('username', 'root');
//ORM::configure('password', 'lika4482**');
//}
date_default_timezone_set("Europe/Belgrade");
defined("TimeZone") ? null : define("TimeZone", " +0100");

require_once(ROOT_DIR . 'app/bootstrap/start.php');