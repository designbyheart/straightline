<?php
/**
 * Created by IntelliJ IDEA.
 * User: Pedja
 * Date: 7/20/15
 * Time: 7:41 PM
 */


session_start();
$app = new \Slim\Slim();
$filteredRoots = array('api');
$firstElement = explode('/', $_SERVER['REQUEST_URI']);

$app->config(array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => './current/app/templates',
));
$app->view->parserExtensions = array(
    new Twig_Extensions_Extension_Text()
);

function api($data)
{
    global $app;
    $app->render(200, $data);
}

function now($date = NULL)
{
    return date('Y-m-d H:i:s', isset($date) ? strtotime($date) : time());
}

function autorize($tokenData)
{
    global $app;
    $token = false;
    if (is_array($token)) {
        if (!isset($tokenData['token'])) {
            $app->render(array("error" => "Can't authorize user"));
            return;
        } else {
            $token = $tokenData['token'];
        }
    } else {
        $token = $tokenData;
    }
    //TODO authorize user
}

//require_once(ROOT_DIR . 'app/bootstrap/routes.php');
require_once(ROOT_DIR . 'app/bootstrap/routes.php');
$app->run();

function pr($item)
{
    print_r($item);
    exit();
}
