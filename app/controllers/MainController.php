<?php
/**
 * Created by IntelliJ IDEA.
 * User: Pedja
 * Date: 7/20/15
 * Time: 8:14 PM
 */

namespace Straightline;


class MainController {

    public static function basicSettings($inputArr = NULL){
        $base = [
            'public'=>SITE_ROOT.'current/public/',
            'root'=>SITE_ROOT,
            'assets'=>SITE_ROOT.'current/public/'
        ];
        return !$inputArr?$base : array_merge($inputArr, $base);
    }
    public static function login(){
        return self::basicSettings([
            'page'=>'login'
        ]);
    }
}