set :application, 'straightline'
set :repo_url, 'git@bitbucket.org:designbyheart/straightline.git'

set :branch, 'master'
set :deploy_via, :remote_cache
set :scm, :git

# Seems to require 'role :app'
#set :linked_files, %w{config/local.php db/flaming-archer.db public/feed.xml}
#set :linked_dirs, %w{var/www/images/}

set :deploy_to, '/var/www/html'
set :shared_children,'var/www/html/images'

set :user, 'root'
set :owner_name, 'root'
set :owner_group, 'root'

task :change_owner do
  try_sudo "chown #{owner_name}:#{owner_group} -R #{deploy_to}"
  try_sudo "chown #{owner_name}:#{owner_group} -R #{shared_path}"
  if !group_writable
    try_sudo "chown #{owner_name}:#{owner_group} #{deploy_to}"
  end
end

set :keep_releases, 5

namespace :deploy do

  desc 'composer install'
  task :composer_install do
    on roles(:web) do
      within release_path do
        execute 'composer', 'install', '--no-dev', '--optimize-autoloader'
      end
    end
  end

  after :updated, 'deploy:composer_install'

  desc 'restart server'
  task :restartServer do
    on roles(:web) do
      within release_path do
        execute 'sudo', 'service', 'apache2', 'restart'
      end
    end
  end
#    after :composer_install, 'deploy:restartServer'

  desc 'Restart application - does nothing, see comments below'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
    # This is present b/c 'cap production deploy' is blowing up w/o it.
    # Not sure what's up with that, the Google hasn't helped, and I'm tired
    # of screwing with it.  It stays in for now.
  end
end
end
