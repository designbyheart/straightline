# config valid only for Capistrano 3.1
#lock '3.1.0'

set :application, 'pennylane'
set :repo_url, 'git@bitbucket.org:designbyheart/penny.git'
# set :user, "root"
# set :password, "my-real-password-here"

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :branch, "master"
set :deploy_via, :remote_cache
set :deploy_to, '/var/www/'
set :shared_children,'var/www/images/gallery'
set :shared_children,'var/www/vendor'

set :user, 'root'
set :owner_name, 'root'
set :owner_group, 'root'

task :change_owner do
  try_sudo "chown #{owner_name}:#{owner_group} -R #{deploy_to}"
  try_sudo "chown #{owner_name}:#{owner_group} -R #{shared_path}"
  if !group_writable
    try_sudo "chown #{owner_name}:#{owner_group} #{deploy_to}"
  end
end

# after 'deploy', 'change_owner'
after 'deploy', 'deploy:cleanup'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# Default value for keep_releases is 5
set :keep_releases, 5
# namespace :deploy do
  # desc "Recreate symlink"
  #  task :resymlink, :roles => :app do
  #   run "rm -f #{current_path} && ln -s #{release_path} #{current_path}"
  # end
# end

#after "deploy:create_symlink",  after "deploy:resymlink"
desc 'composer install'
task :composer_install do
    on roles(:web) do
        within release_path do
            execute 'composer', 'install', '--no-dev', '--optimize-autoloader'
        end
    end
end

after :updated, 'deploy:composer_install'

namespace :deploy do
  before :starting,'composer:install_executable'
end
