<?php

namespace Straightline;

Class Controller extends \Slim\Slim
{
    protected $data;

    public function __construct()
    {
        $settings = require(WEBROOT."library/settings.php");
        if (isset($settings['model'])) {
            $this->data = $settings['model'];
        }
        parent::__construct($settings);
    }

    public function render($name, $data = array(), $status = null)
    {
        if (strpos($name, ".twig") === false) {
            $name = $name . ".twig";
        }
        parent::render($name, $data, $status);
    }
    public function setMainData($title, $subtitle,$page){
        $mainData = array(
            'title'=>$title,
            'subtitle'=>$subtitle,
            'page'=>$page
        );
        return $mainData;
    }
}
